<?php
/**
 * Created by PhpStorm.
 * User: npokfi
 * Date: 2016-05-19
 * Time: 10:18
 */
error_reporting(E_ALL);

$loginString = $_SERVER['AUTH_USER'];

//example login strings
//$loginString = 'DS\11105';
//$loginString = 'DS\SA-5105-001';




interface IPrepareStoreNumber {

    public function findStoreNumberWithRegex();
    public function setProperStoreName();

}

class UserName implements IPrepareStoreNumber {
    private $unpreparedStoreName;

    function __construct($givenString = null)
    {
        $this->unpreparedStoreName = $givenString;
    }

    public function findStoreNumberWithRegex()
    {
        $loginString = $this->unpreparedStoreName;
        if(preg_match('/\d{4,5}/', $loginString, $output_array)) {
            return $output_array[0];
        }
    }
   public function setProperStoreName()
   {
       if(is_numeric($this->findStoreNumberWithRegex())) {
           return  substr($this->findStoreNumberWithRegex(), -3);
       } return '999';

   }
}

$storeName = new UserName($loginString);
$storeName->findStoreNumberWithRegex();
$storeNumberParsed = $storeName->setProperStoreName();